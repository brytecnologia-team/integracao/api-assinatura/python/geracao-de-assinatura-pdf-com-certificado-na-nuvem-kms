# API de Geração de assinatura de PDF com certificado em nuvem

Este exemplo apresenta os passos necessários para a geração de assinatura de PDF utilizando-se do certificado em nuvem.

### Formas de acesso ao Certificado em Nuvem

* *BRy KMS*: Certificado está armazenado na Plataforma de Certificados em Nuvem da BRy Tecnologia. Para este modo de acesso ao certificado o Tipo da Credencial selecionado deve ser **PIN** ou **TOKEN**. Para credenciais com tipo **PIN** o Valor da Credencial corresponde a senha de acesso ao certificado em nuvem, enquanto que para credenciais do tipo **TOKEN** o Valor da Credencial corresponde a um token de autorização do acesso ao certificado. Mais informações sobre o serviço de certificados em nuvem podem ser encontradas em nossa documentação da [API de Certificado em Nuvem](https://api-assinatura.bry.com.br/api-certificado-em-nuvem).

* *GovBR*: Certificado está armazenado na Plataforma de Assinatura Digital gov.br. Para este modo de acesso ao certificado o Tipo da Credencial selecionado deve ser **GOVBR** e o Valor da Credencial corresponde ao token de acesso do usuário. É possível verificar um exemplo da geração deste token de acesso em nossa [API Autenticação GOV.BR](https://gitlab.com/brytecnologia-team/integracao/api-autenticacao-token-govbr/react/gestao-credenciais).

* *HSM Dinamo*: Certificado está armazenado em um HSM Dinamo. Para este modo de acesso ao certificado o Tipo da Credencial selecionado deve ser **DINAMO** e o Valor da Credencial corresponde ao token de acesso, PIN da conta, ou OTP do usuário. É possível verificar um exemplo de como usar as credenciais do DINAMO em nosso [Repositório Swagger da API de Assinatura](https://hub2.bry.com.br/swagger-ui/index.html#/) no schema kms_data.

### Tech

O exemplo utiliza das bibliotecas Python abaixo:
* [Requests] - HTTP for Humans!
* [Flask] - Flask (source code) is a Python Web framework designed to receive HTTP requests.
* [Flask-restful] - Flask-RESTful is an extension for Flask that adds support for quickly building REST APIs
* [Flask-Cors] - A Flask extension for handling Cross Origin Resource Sharing (CORS), making cross-origin AJAX possible.
* [Python] - Python 3.8

**Observação**

Esta API em [Python] pode ser executada juntamente com o [FrontEnd](https://gitlab.com/brytecnologia-team/integracao/api-assinatura/react/assinatura-pdf-kms) de exemplo feito em React.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

**Observação**

É necessário ter o [Python] e todas suas dependências instalados na sua máquina. Também é necessário executar esta aplicação juntamente com o frontend.

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| AUTHORIZATION | Token de acesso (access_token), obtido no BRyCloud (https://api-assinatura.bry.com.br/api-assinatura-digital#autenticacao-apis). Para uso corporativo, o token deve ser da pessoa jurídica e para uso pessoal, da pessoa física. | pdf_signer.py
| URI_HUB | Uri de acesso ao serviço BRyHUB-Signer. | pdf_signer.py

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. Utilizamos o Python versão 3.8 e pip3 para a instalação das dependências.

Comandos:

Instalar Flask:

    -pip3 install Flask

Instalar Flask-Cors

    -pip3 install -U flask-cors

Instalar Flask-Restful:

    -pip3 install flask-restful

Executar programa:

    -python3 pdf_signer.py



   [Requests]: <https://pypi.org/project/requests/>
   [Flask]: <https://palletsprojects.com/p/flask/>
   [Flask-restful]:<https://flask-restful.readthedocs.io/en/latest/> 
   [Flask-Cors]: <https://flask-cors.readthedocs.io/en/latest/>
   [Python]: <https://www.python.org/downloads/release/python-380/>
