from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask_cors import CORS
import json
import requests
import base64

app = Flask(__name__)
CORS(app)
api = Api(app)

# Token de autenticação gerado no BRy Cloud
AUTHORIZATION = ''

# Endereço de acesso ao HUB-Signer
# URI_HUB = 'https://hub2.hom.bry.com.br'   #Ambiente de homologação
URI_HUB = 'https://hub2.bry.com.br'         #Ambiente de Produção


class assina_KMS(Resource):
    def post(self):
        # Pega o conteúdo que vem da requisição do front-end
        data = request.form

        # Lê o documento enviado para assinatura no front-end
        pdf_assinatura = request.files['documento'].read()

        # Recupera o tipo da credencial enviado (BRYKMS, DINAMO ou GOVBR)
        kms_type = data["kms_type"]

        # Recupera o valor da credencial
        kms_credencial = data["valor_credencial"]

        # Tipo da credencial dentro do sistema de hospedagem de certificados (PIN, TOKEN ou OTP)
        kms_type_credential =  data['kms_type_credential'];

        # Dados das credenciais do KMS a serem enviados na requisição
        kms_data_info = ""

        # Recuperação dos dados de cada tipo de credencial
        if kms_type_credential == "PIN":
            kms_data_info =  '"pin" : "' + kms_credencial + '"'
        elif kms_type_credential == "TOKEN" : # Caso seja GOVBR é sempre token
            kms_data_info =  '"token" : "' + kms_credencial + '"'
        elif kms_type_credential == "OTP" :
            kms_data_info =  '"otp" : "' + kms_credencial + '"'

        dataKmsCredenciaisSistema = ""
        if kms_type == "DINAMO":
            if 'user_pkey' in data:
                dataKmsCredenciaisSistema = dataKmsCredenciaisSistema + ", " + '"user_pkey" : "' + data['user_pkey'] + '"'
            if 'user' in data:
                dataKmsCredenciaisSistema = dataKmsCredenciaisSistema + ", " + '"user" : "' + data['user'] + '"'
            if 'uuid_cert' in data:
                dataKmsCredenciaisSistema = dataKmsCredenciaisSistema + ", " + '"uuid_cert" : "' + data['uuid_cert'] + '"'
        elif kms_type == "BRYKMS":
             if 'user' in data:
                dataKmsCredenciaisSistema = dataKmsCredenciaisSistema + ", " + '"user" : "' + data['user'] + '"'
             if 'uuid_cert' in data:
                dataKmsCredenciaisSistema = dataKmsCredenciaisSistema + ", " + '"uuid_cert" : "' + data['uuid_cert'] + '"'
        
        kms_data_info = '{' + kms_data_info + dataKmsCredenciaisSistema + '}'

        # Verifica se a assinatura possuí imagem e realiza a requisição de acordo com essa condição
        if data['incluirIMG'] == "true":
            image = request.files['imagem'].read()
            signer_form = { # Valor do campo signatario deve ser colocado entre " " na requisicao
                'dados_assinatura': '{"algoritmoHash" : "' + data['algoritmoHash'] + '", "perfil" : "' + data['perfil'] + '"' + '}',
                'configuracao_imagem': '{"altura" : "' + data['altura'] + '",  "largura" : ' + data['largura'] + ',  "coordenadaX" : ' + data['coordenadaX'] + ',  "coordenadaY" : ' + data['coordenadaY'] + ',  "posicao" : "' + data['posicao'] + '", "pagina" : "' + data['pagina'] + '"}',
                'configuracao_texto': '{"texto" : "' + data['texto'] + '" ,"incluirCN" : ' + data['incluirCN'] + ', "incluirCPF" : ' + data['incluirCPF'] + ', "incluirEmail" : ' + data['incluirEmail'] + '}',
                'kms_data': kms_data_info
            }
            files = [
                ('documento', pdf_assinatura),
                ('imagem', image)
            ]
        else:
            signer_form = {
                'dados_assinatura': '{ "algoritmoHash" : "' + data['algoritmoHash'] + '", "perfil" : "' + data['perfil'] + '"' + ' }',
                'kms_data': kms_data_info
            }
            files = [
                ('documento', pdf_assinatura),
            ]
        header = {
            'Authorization': AUTHORIZATION,
            'kms_type': kms_type
        }

        print('============= Iniciando assinatura no BRy HUB utilizando certificado em nuvem ... =============')

        response = requests.post(URI_HUB + '/fw/v1/pdf/kms/lote/assinaturas',
                                 data=signer_form, files=files, headers=header)
        if response.status_code == 200:
            print('Assinatura realizada com sucesso!')
            print(response.json())

            return response.json()['documentos'][0]['links'][0]['href']
        else:
            # Retorna mensagem e status do erro do HUB
            print(response.text)
            return response.text, response.status_code

api.add_resource(assina_KMS, '/assinador/assinarKMS')

if __name__ == '__main__':
    app.run(host="localhost", port=8000, debug=True)
